<?php

namespace Erparom\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        return $this->render('ErparomAdminBundle:Default:index.html.twig');
    }
}
